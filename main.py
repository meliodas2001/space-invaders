import pygame
import random
from pygame import mixer
from space_invaders.constants import WIDTH, HEIGHT, ICON, BACKGROUND, WINDOW, MAX_ALIENS, BULLET_SOUND, FPS, CLOCK, HIT_SOUND
from space_invaders.enemy import Enemy
from space_invaders.player import Player
from space_invaders.projectile import Projectile
from space_invaders.functions import redraw_game_window

pygame.display.set_caption("Space Invaders")
pygame.display.set_icon(ICON)

music = mixer.music.load('assests/background.wav')
# mixer.music.play(-1)

def main():
    run = True
    score = 0
    ship = Player(500, 700, 64, 64)
    aliens = []
    for i in range(MAX_ALIENS):
        aliens.append(Enemy(random.randint(0, 950), random.randint(50, 150), 64, 64))
        bullet = Projectile(aliens[i], ship.x, 700, 32, 32)

    while run:
        CLOCK.tick(FPS)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

        keys = pygame.key.get_pressed()

        if keys[pygame.K_LEFT] and ship.x > ship.vel:
            ship.x -= ship.vel 

        if keys[pygame.K_RIGHT] and ship.x < WIDTH - ship.width - ship.vel:
            ship.x += ship.vel

        if keys[pygame.K_SPACE] and bullet.state == 'ready':
            bullet.x = ship.x 
            bullet.draw(WINDOW)
            BULLET_SOUND.play()

        if bullet.y == 0:
            bullet.y = 480
            bullet.state = 'ready'

        if bullet.state == 'fire':
            bullet.y -= bullet.vel
            bullet.draw(WINDOW)
            pygame.display.update()

        for i in range(MAX_ALIENS):
            collision = bullet.hit(aliens[i])
            if collision:
                HIT_SOUND.play()
                bullet.y = 480
                bullet.state = 'ready'
                score += 10
                aliens[i].x = random.randint(0, 735)
                aliens[i].y = random.randint(50, 150)

        redraw_game_window(WINDOW, score, ship, aliens)

if __name__ == '__main__':
    main()

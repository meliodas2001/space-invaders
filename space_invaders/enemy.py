import pygame
from space_invaders.constants import ENEMY_IMAGE_1
from space_invaders.functions import game_over

class Enemy:
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.vel = 2
        self.hitbox = (self.x, self.y, 64, 55)
        self.visible = True
        self.num = 6

    def draw(self, win):
        self.move()
        if self.visible:
            for i in range(self.num):
                for j in range(self.num):
                    if self.y > 650:
                        game_over(win)
                        self.y = 900
                        break

                win.blit(ENEMY_IMAGE_1, (self.x, self.y))
                self.hitbox = (self.x, self.y, 64, 55)
                pygame.draw.rect(win, (255,0,0), self.hitbox, 2)

    def move(self):
        for i in range(self.num):
            self.x += self.vel
            if self.x <= 0:
                self.vel = 2
                self.x += self.vel
                self.y += 30
            elif self.x >= 930:
                self.vel = -2
                self.x += self.vel
                self.y += 30

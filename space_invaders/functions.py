import pygame
from space_invaders.constants import ALGERIAN_FONT_50, BACKGROUND1, WHITE, YELLOW

pygame.init()

def game_over(window):
    text = ALGERIAN_FONT_50.render('GAME OVER', 1, WHITE)
    window.blit(text, (250,300))

def redraw_game_window(window, score, ship, aliens):
    window.blit(BACKGROUND1, (0, 0))
    text = ALGERIAN_FONT_50.render('Score: ' + str(score), 1, YELLOW)
    window.blit(text, (50,10))
    ship.draw(window)
    for i in range(6):
        aliens[i].draw(window)
    pygame.display.update()

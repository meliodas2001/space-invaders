import pygame
from pygame import mixer

pygame.init()

WIDTH = 1024
HEIGHT = 768
FPS = 60
MAX_ALIENS = 8

RED    = (255,   0,   0)
WHITE  = (255, 255, 255)
BLACK  = (  0,   0,   0)
BLUE   = (  0,   0, 255)
GREY   = (128, 128, 128)
GREEN  = (  0, 256,   0)
YELLOW = (255, 255,   0)

WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))
CLOCK = pygame.time.Clock()

ICON = pygame.image.load('assests/rocket.png')
background = pygame.image.load('assests/background.png')
background1 = pygame.image.load('assests/background2.jpg')
BACKGROUND = pygame.transform.scale(background, (WIDTH, HEIGHT)).convert_alpha()
BACKGROUND1 = pygame.transform.scale(background1, (WIDTH, HEIGHT)).convert_alpha()
PLAYER_IMAGE = pygame.image.load('assests/player.png')
ENEMY_IMAGE_1 = pygame.image.load('assests/enemy1.png')
ENEMY_IMAGE_2 = pygame.image.load('assests/enemy2.png')
ENEMY_IMAGE_3 = pygame.image.load('assests/enemy3.png')
BULLET_IMAGE = pygame.image.load('assests/bullet.png')

BULLET_SOUND = mixer.Sound('assests/laser.wav')
HIT_SOUND = mixer.Sound('assests/explosion.wav')

ALGERIAN_FONT_50 = pygame.font.SysFont('Algerian', 50, True, True)
ALGERIAN_FONT_30 = pygame.font.SysFont('Algerian', 30, True, True)
